# BT Relay - Board

This is a simple board create with Cadsoft Eagle that features a 5V/10A relay,
a PIC microcontroller and a bluetooth module for serial communication (HC-05)
(compatible with the Bluetooth 2.0 Serial Port Profile).

The bluetooth module can be easily changed to any other serial module,
like USB/Serial module, Zigbee module, etc., as the microcontroller is
just expecting async serial data (TX & RX signals).